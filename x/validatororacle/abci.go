package validatororacle

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/telemetry"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/mayachain/aztec/x/validatororacle/keeper"
	types "gitlab.com/mayachain/aztec/x/validatororacle/types"
)

// EndBlocker is called at the end of every block
func EndBlocker(ctx sdk.Context, k keeper.Keeper) {
	defer telemetry.ModuleMeasureSince(types.ModuleName, time.Now(), telemetry.MetricKeyEndBlocker)

	params := k.GetParams(ctx)
	if types.IsPeriodLastBlock(ctx, params.VotePeriod) {

		// Build claim map over all validators in active set
		validatorClaimMap := make(map[string]types.Claim)

		maxValidators := k.StakingKeeper.MaxValidators(ctx)
		iterator := k.StakingKeeper.ValidatorsPowerStoreIterator(ctx)
		defer iterator.Close()

		powerReduction := k.StakingKeeper.PowerReduction(ctx)

		counter := 0
		activeValidators := 0

		for ; iterator.Valid() && counter < int(maxValidators); iterator.Next() {
			validator := k.StakingKeeper.Validator(ctx, iterator.Value())

			// Exclude not bonded validator
			if validator.IsBonded() && !validator.IsJailed() {
				valAddr := validator.GetOperator()
				fmt.Println("valAddr", valAddr.String())
				validatorClaimMap[valAddr.String()] = types.NewClaim(validator.GetConsensusPower(powerReduction), 0, 0, valAddr)
				activeValidators++
			}
			counter++
		}

		fmt.Println("Active validators", activeValidators)

		// Clear all validators from whitelist
		validators := k.IterateAddressWhitelist(ctx)

		for _, val := range validators {
			k.DeleteAddressFromWhitelist(ctx, val)

		}

		// Count votes per Address
		voteMap, countVotes := k.OrganizeBallotByAddress(ctx, validatorClaimMap)

		fmt.Println("voters", len(voteMap))
		fmt.Println("voteMap", voteMap)
		fmt.Println("countVotes", countVotes)

		//Select address that have more than 2/3 of votes
		validAdress := Tally(activeValidators, countVotes)

		//add validate address to the whitelist
		countAddressWhitelist := 0
		for _, addr := range validAdress {
			countAddressWhitelist++
			k.SetAddressWhitelistWithEvent(ctx, addr)
		}

		fmt.Println("count validators", countAddressWhitelist)

		max_points := uint64(countAddressWhitelist)

		if max_points > 0 {
			for voter, votes := range voteMap {

				countCorrectVotes := 0
				countIncorrectVotes := 0
				addr, err := sdk.ValAddressFromBech32(voter)

				if err != nil {
					fmt.Println(err.Error())
					continue
				}

				// if the validator don´t vote
				if len(votes) == 0 {
					k.SetMissCounter(ctx, addr, sdk.OneDec())
					continue
				}

				for _, vote := range votes {
					if ok, _ := k.GetAddressWhitelist(ctx, vote); ok {
						countCorrectVotes++
					} else {
						countIncorrectVotes++
					}
				}

				totalPoints := max_points - uint64(countCorrectVotes) + uint64(countIncorrectVotes)

				if totalPoints > 0 {
					totalPoints -= 1
				}

				points := sdk.NewDec(int64(totalPoints))
				points = points.Quo(sdk.NewDec(int64(countAddressWhitelist)))
				fmt.Println("slashing points", points.String())
				k.SetMissCounter(ctx, addr, points)

			}
		}
		k.SlashAndResetMissCounters(ctx)
		k.ClearBallots(ctx, params.VotePeriod)

	}

	// start validate after 5000 height
	if ctx.BlockHeight() > 5000 {

		validators := k.StakingKeeper.GetAllValidators(ctx)

		for _, validator := range validators {
			if ok, _ := k.GetAddressWhitelist(ctx, validator.String()); !ok {
				//k.StakingKeeper.Jail(ctx, validator)
				fmt.Println("Address is not in the withelist : ", validator.OperatorAddress)
			}
		}
	}

}

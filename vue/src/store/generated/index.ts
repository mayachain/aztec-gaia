// THIS FILE IS GENERATED AUTOMATICALLY. DO NOT MODIFY.

import aztecGlobalfeeV1Beta1 from './aztec.globalfee.v1beta1'
import aztecIcamauthV1Beta1 from './aztec.icamauth.v1beta1'
import aztecValidatororacleV1Beta1 from './aztec.validatororacle.v1beta1'


export default { 
  aztecGlobalfeeV1Beta1: load(aztecGlobalfeeV1Beta1, 'aztec.globalfee.v1beta1'),
  aztecIcamauthV1Beta1: load(aztecIcamauthV1Beta1, 'aztec.icamauth.v1beta1'),
  aztecValidatororacleV1Beta1: load(aztecValidatororacleV1Beta1, 'aztec.validatororacle.v1beta1'),
  
}


function load(mod, fullns) {
    return function init(store) {        
        if (store.hasModule([fullns])) {
            throw new Error('Duplicate module name detected: '+ fullns)
        }else{
            store.registerModule([fullns], mod)
            store.subscribe((mutation) => {
                if (mutation.type == 'common/env/INITIALIZE_WS_COMPLETE') {
                    store.dispatch(fullns+ '/init', null, {
                        root: true
                    })
                }
            })
        }
    }
}
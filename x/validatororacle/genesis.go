package validatororacle

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"gitlab.com/mayachain/aztec/x/validatororacle/keeper"
	"gitlab.com/mayachain/aztec/x/validatororacle/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, data *types.GenesisState) {
	for _, ex := range data.WhitelistValidators {
		k.SetAddressWhitelist(ctx, ex.String())
	}

	for _, mc := range data.MissCounters {
		operator, err := sdk.ValAddressFromBech32(mc.ValidatorAddress)
		if err != nil {
			panic(err)
		}

		k.SetMissCounter(ctx, operator, sdk.NewDec(int64(mc.MissCounter)))
	}

	for _, ap := range data.AggregateValidatorPrevotes {
		valAddr, err := sdk.ValAddressFromBech32(ap.Voter)
		if err != nil {
			panic(err)
		}

		k.SetAggregateValidatorPrevote(ctx, valAddr, ap)
	}

	for _, av := range data.AggregateValidatorVotes {
		valAddr, err := sdk.ValAddressFromBech32(av.Voter)
		if err != nil {
			panic(err)
		}

		k.SetAggregateValidatorVote(ctx, valAddr, av)
	}

	fmt.Println(data)
	k.SetParams(ctx, data.Params)
	// this line is used by starport scaffolding # genesis/module/init

}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()
	genesis.Params = k.GetParams(ctx)

	// this line is used by starport scaffolding # genesis/module/export

	return genesis
}

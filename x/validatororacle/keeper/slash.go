package keeper

import (
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
)

func (k Keeper) SlashAndResetMissCounters(ctx sdk.Context) {
	height := ctx.BlockHeight()
	distributionHeight := height - sdk.ValidatorUpdateDelay - 1

	slashFraction := k.SlashFraction(ctx)
	powerReduction := k.StakingKeeper.PowerReduction(ctx)
	ref := sdk.MustNewDecFromStr("0.7")

	fmt.Println("dist height", distributionHeight)
	fmt.Println("slashFraction ", slashFraction)

	k.IterateMissCounters(ctx, func(operator sdk.ValAddress, missCounter sdk.Dec) bool {

		// Calculate valid vote rate; (SlashWindow - MissCounter)/SlashWindow
		validator := k.StakingKeeper.Validator(ctx, operator)
		if validator.IsBonded() && !validator.IsJailed() {
			consAddr, err := validator.GetConsAddr()
			if err != nil {
				panic(err)
			}
			if !missCounter.IsZero() {

				k.StakingKeeper.Slash(
					ctx, consAddr,
					distributionHeight, validator.GetConsensusPower(powerReduction), slashFraction.Mul(missCounter),
				)

				if missCounter.GTE(ref) {
					k.StakingKeeper.Jail(ctx, consAddr)
				}

			}

		}

		k.DeleteMissCounter(ctx, operator)
		return false
	})

}

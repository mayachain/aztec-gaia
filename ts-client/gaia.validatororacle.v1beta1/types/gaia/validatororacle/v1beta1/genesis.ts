/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";
import { AggregateValidatorPrevote, AggregateValidatorVote, Params, Validator } from "./params";

export const protobufPackage = "aztec.validatororacle.v1beta1";

/** GenesisState defines the oraclevalidator module's genesis state. */
export interface GenesisState {
  params: Params | undefined;
  whitelistValidators: Validator[];
  missCounters: MissCounter[];
  aggregateValidatorPrevotes: AggregateValidatorPrevote[];
  /** this line is used by starport scaffolding # genesis/proto/state */
  aggregateValidatorVotes: AggregateValidatorVote[];
}

/**
 * MissCounter defines an miss counter and validator address pair used in
 * oracle module's genesis state
 */
export interface MissCounter {
  validatorAddress: string;
  missCounter: number;
}

function createBaseGenesisState(): GenesisState {
  return {
    params: undefined,
    whitelistValidators: [],
    missCounters: [],
    aggregateValidatorPrevotes: [],
    aggregateValidatorVotes: [],
  };
}

export const GenesisState = {
  encode(message: GenesisState, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.params !== undefined) {
      Params.encode(message.params, writer.uint32(10).fork()).ldelim();
    }
    for (const v of message.whitelistValidators) {
      Validator.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.missCounters) {
      MissCounter.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    for (const v of message.aggregateValidatorPrevotes) {
      AggregateValidatorPrevote.encode(v!, writer.uint32(42).fork()).ldelim();
    }
    for (const v of message.aggregateValidatorVotes) {
      AggregateValidatorVote.encode(v!, writer.uint32(50).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): GenesisState {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseGenesisState();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.params = Params.decode(reader, reader.uint32());
          break;
        case 2:
          message.whitelistValidators.push(Validator.decode(reader, reader.uint32()));
          break;
        case 4:
          message.missCounters.push(MissCounter.decode(reader, reader.uint32()));
          break;
        case 5:
          message.aggregateValidatorPrevotes.push(AggregateValidatorPrevote.decode(reader, reader.uint32()));
          break;
        case 6:
          message.aggregateValidatorVotes.push(AggregateValidatorVote.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): GenesisState {
    return {
      params: isSet(object.params) ? Params.fromJSON(object.params) : undefined,
      whitelistValidators: Array.isArray(object?.whitelistValidators)
        ? object.whitelistValidators.map((e: any) => Validator.fromJSON(e))
        : [],
      missCounters: Array.isArray(object?.missCounters)
        ? object.missCounters.map((e: any) => MissCounter.fromJSON(e))
        : [],
      aggregateValidatorPrevotes: Array.isArray(object?.aggregateValidatorPrevotes)
        ? object.aggregateValidatorPrevotes.map((e: any) => AggregateValidatorPrevote.fromJSON(e))
        : [],
      aggregateValidatorVotes: Array.isArray(object?.aggregateValidatorVotes)
        ? object.aggregateValidatorVotes.map((e: any) => AggregateValidatorVote.fromJSON(e))
        : [],
    };
  },

  toJSON(message: GenesisState): unknown {
    const obj: any = {};
    message.params !== undefined && (obj.params = message.params ? Params.toJSON(message.params) : undefined);
    if (message.whitelistValidators) {
      obj.whitelistValidators = message.whitelistValidators.map((e) => e ? Validator.toJSON(e) : undefined);
    } else {
      obj.whitelistValidators = [];
    }
    if (message.missCounters) {
      obj.missCounters = message.missCounters.map((e) => e ? MissCounter.toJSON(e) : undefined);
    } else {
      obj.missCounters = [];
    }
    if (message.aggregateValidatorPrevotes) {
      obj.aggregateValidatorPrevotes = message.aggregateValidatorPrevotes.map((e) =>
        e ? AggregateValidatorPrevote.toJSON(e) : undefined
      );
    } else {
      obj.aggregateValidatorPrevotes = [];
    }
    if (message.aggregateValidatorVotes) {
      obj.aggregateValidatorVotes = message.aggregateValidatorVotes.map((e) =>
        e ? AggregateValidatorVote.toJSON(e) : undefined
      );
    } else {
      obj.aggregateValidatorVotes = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<GenesisState>, I>>(object: I): GenesisState {
    const message = createBaseGenesisState();
    message.params = (object.params !== undefined && object.params !== null)
      ? Params.fromPartial(object.params)
      : undefined;
    message.whitelistValidators = object.whitelistValidators?.map((e) => Validator.fromPartial(e)) || [];
    message.missCounters = object.missCounters?.map((e) => MissCounter.fromPartial(e)) || [];
    message.aggregateValidatorPrevotes =
      object.aggregateValidatorPrevotes?.map((e) => AggregateValidatorPrevote.fromPartial(e)) || [];
    message.aggregateValidatorVotes = object.aggregateValidatorVotes?.map((e) => AggregateValidatorVote.fromPartial(e))
      || [];
    return message;
  },
};

function createBaseMissCounter(): MissCounter {
  return { validatorAddress: "", missCounter: 0 };
}

export const MissCounter = {
  encode(message: MissCounter, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.validatorAddress !== "") {
      writer.uint32(10).string(message.validatorAddress);
    }
    if (message.missCounter !== 0) {
      writer.uint32(16).uint64(message.missCounter);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MissCounter {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMissCounter();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.validatorAddress = reader.string();
          break;
        case 2:
          message.missCounter = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MissCounter {
    return {
      validatorAddress: isSet(object.validatorAddress) ? String(object.validatorAddress) : "",
      missCounter: isSet(object.missCounter) ? Number(object.missCounter) : 0,
    };
  },

  toJSON(message: MissCounter): unknown {
    const obj: any = {};
    message.validatorAddress !== undefined && (obj.validatorAddress = message.validatorAddress);
    message.missCounter !== undefined && (obj.missCounter = Math.round(message.missCounter));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MissCounter>, I>>(object: I): MissCounter {
    const message = createBaseMissCounter();
    message.validatorAddress = object.validatorAddress ?? "";
    message.missCounter = object.missCounter ?? 0;
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

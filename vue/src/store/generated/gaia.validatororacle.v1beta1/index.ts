import { Client, registry, MissingWalletError } from 'aztec-v8-client-ts'

import { MissCounter } from "aztec-v8-client-ts/aztec.validatororacle.v1beta1/types"
import { Params } from "aztec-v8-client-ts/aztec.validatororacle.v1beta1/types"
import { Validator } from "aztec-v8-client-ts/aztec.validatororacle.v1beta1/types"
import { AggregateValidatorPrevote } from "aztec-v8-client-ts/aztec.validatororacle.v1beta1/types"
import { AggregateValidatorVote } from "aztec-v8-client-ts/aztec.validatororacle.v1beta1/types"


export { MissCounter, Params, Validator, AggregateValidatorPrevote, AggregateValidatorVote };

function initClient(vuexGetters) {
	return new Client(vuexGetters['common/env/getEnv'], vuexGetters['common/wallet/signer'])
}

function mergeResults(value, next_values) {
	for (let prop of Object.keys(next_values)) {
		if (Array.isArray(next_values[prop])) {
			value[prop]=[...value[prop], ...next_values[prop]]
		}else{
			value[prop]=next_values[prop]
		}
	}
	return value
}

type Field = {
	name: string;
	type: unknown;
}
function getStructure(template) {
	let structure: {fields: Field[]} = { fields: [] }
	for (const [key, value] of Object.entries(template)) {
		let field = { name: key, type: typeof value }
		structure.fields.push(field)
	}
	return structure
}
const getDefaultState = () => {
	return {
				Params: {},
				Actives: {},
				FeederDelegation: {},
				AggregatePrevote: {},
				AggregatePrevotes: {},
				AggregateVote: {},
				AggregateVotes: {},
				
				_Structure: {
						MissCounter: getStructure(MissCounter.fromPartial({})),
						Params: getStructure(Params.fromPartial({})),
						Validator: getStructure(Validator.fromPartial({})),
						AggregateValidatorPrevote: getStructure(AggregateValidatorPrevote.fromPartial({})),
						AggregateValidatorVote: getStructure(AggregateValidatorVote.fromPartial({})),
						
		},
		_Registry: registry,
		_Subscriptions: new Set(),
	}
}

// initial state
const state = getDefaultState()

export default {
	namespaced: true,
	state,
	mutations: {
		RESET_STATE(state) {
			Object.assign(state, getDefaultState())
		},
		QUERY(state, { query, key, value }) {
			state[query][JSON.stringify(key)] = value
		},
		SUBSCRIBE(state, subscription) {
			state._Subscriptions.add(JSON.stringify(subscription))
		},
		UNSUBSCRIBE(state, subscription) {
			state._Subscriptions.delete(JSON.stringify(subscription))
		}
	},
	getters: {
				getParams: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.Params[JSON.stringify(params)] ?? {}
		},
				getActives: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.Actives[JSON.stringify(params)] ?? {}
		},
				getFeederDelegation: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.FeederDelegation[JSON.stringify(params)] ?? {}
		},
				getAggregatePrevote: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.AggregatePrevote[JSON.stringify(params)] ?? {}
		},
				getAggregatePrevotes: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.AggregatePrevotes[JSON.stringify(params)] ?? {}
		},
				getAggregateVote: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.AggregateVote[JSON.stringify(params)] ?? {}
		},
				getAggregateVotes: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.AggregateVotes[JSON.stringify(params)] ?? {}
		},
				
		getTypeStructure: (state) => (type) => {
			return state._Structure[type].fields
		},
		getRegistry: (state) => {
			return state._Registry
		}
	},
	actions: {
		init({ dispatch, rootGetters }) {
			console.log('Vuex module: aztec.validatororacle.v1beta1 initialized!')
			if (rootGetters['common/env/client']) {
				rootGetters['common/env/client'].on('newblock', () => {
					dispatch('StoreUpdate')
				})
			}
		},
		resetState({ commit }) {
			commit('RESET_STATE')
		},
		unsubscribe({ commit }, subscription) {
			commit('UNSUBSCRIBE', subscription)
		},
		async StoreUpdate({ state, dispatch }) {
			state._Subscriptions.forEach(async (subscription) => {
				try {
					const sub=JSON.parse(subscription)
					await dispatch(sub.action, sub.payload)
				}catch(e) {
					throw new Error('Subscriptions: ' + e.message)
				}
			})
		},
		
		
		
		 		
		
		
		async QueryParams({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryParams()).data
				
					
				commit('QUERY', { query: 'Params', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryParams', payload: { options: { all }, params: {...key},query }})
				return getters['getParams']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryParams API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryActives({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryActives()).data
				
					
				commit('QUERY', { query: 'Actives', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryActives', payload: { options: { all }, params: {...key},query }})
				return getters['getActives']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryActives API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryFeederDelegation({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryFeederDelegation( key.validator_addr)).data
				
					
				commit('QUERY', { query: 'FeederDelegation', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryFeederDelegation', payload: { options: { all }, params: {...key},query }})
				return getters['getFeederDelegation']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryFeederDelegation API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryAggregatePrevote({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryAggregatePrevote( key.validator_addr)).data
				
					
				commit('QUERY', { query: 'AggregatePrevote', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryAggregatePrevote', payload: { options: { all }, params: {...key},query }})
				return getters['getAggregatePrevote']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryAggregatePrevote API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryAggregatePrevotes({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryAggregatePrevotes()).data
				
					
				commit('QUERY', { query: 'AggregatePrevotes', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryAggregatePrevotes', payload: { options: { all }, params: {...key},query }})
				return getters['getAggregatePrevotes']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryAggregatePrevotes API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryAggregateVote({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryAggregateVote( key.validator_addr)).data
				
					
				commit('QUERY', { query: 'AggregateVote', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryAggregateVote', payload: { options: { all }, params: {...key},query }})
				return getters['getAggregateVote']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryAggregateVote API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryAggregateVotes({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.aztecValidatororacleV1Beta1.query.queryAggregateVotes()).data
				
					
				commit('QUERY', { query: 'AggregateVotes', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryAggregateVotes', payload: { options: { all }, params: {...key},query }})
				return getters['getAggregateVotes']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryAggregateVotes API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		async sendMsgAggregateValidatorVote({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.aztecValidatororacleV1Beta1.tx.sendMsgAggregateValidatorVote({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgAggregateValidatorVote:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgAggregateValidatorVote:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgDelegateFeedConsent({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.aztecValidatororacleV1Beta1.tx.sendMsgDelegateFeedConsent({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDelegateFeedConsent:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgDelegateFeedConsent:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgAggregateValidatorPrevote({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.aztecValidatororacleV1Beta1.tx.sendMsgAggregateValidatorPrevote({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgAggregateValidatorPrevote:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgAggregateValidatorPrevote:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		
		async MsgAggregateValidatorVote({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.aztecValidatororacleV1Beta1.tx.msgAggregateValidatorVote({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgAggregateValidatorVote:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgAggregateValidatorVote:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgDelegateFeedConsent({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.aztecValidatororacleV1Beta1.tx.msgDelegateFeedConsent({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDelegateFeedConsent:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgDelegateFeedConsent:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgAggregateValidatorPrevote({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.aztecValidatororacleV1Beta1.tx.msgAggregateValidatorPrevote({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgAggregateValidatorPrevote:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgAggregateValidatorPrevote:Create Could not create message: ' + e.message)
				}
			}
		},
		
	}
}

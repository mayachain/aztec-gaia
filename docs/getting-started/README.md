---
order: false
parent:
  order: 2
---

# Getting Started

This folder contains tutorials related to the `aztec` application.

- [Installing `aztecd`](./installation.md)

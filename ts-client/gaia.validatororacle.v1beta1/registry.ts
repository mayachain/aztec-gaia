import { GeneratedType } from "@cosmjs/proto-signing";
import { MsgDelegateFeedConsent } from "./types/aztec/validatororacle/v1beta1/tx";
import { MsgAggregateValidatorPrevote } from "./types/aztec/validatororacle/v1beta1/tx";
import { MsgAggregateValidatorVote } from "./types/aztec/validatororacle/v1beta1/tx";

const msgTypes: Array<[string, GeneratedType]>  = [
    ["/aztec.validatororacle.v1beta1.MsgDelegateFeedConsent", MsgDelegateFeedConsent],
    ["/aztec.validatororacle.v1beta1.MsgAggregateValidatorPrevote", MsgAggregateValidatorPrevote],
    ["/aztec.validatororacle.v1beta1.MsgAggregateValidatorVote", MsgAggregateValidatorVote],
    
];

export { msgTypes }
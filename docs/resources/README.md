---
order: false
parent:
  title: Resources
  order: 8
---

# Resources

This folder contains resources on the `aztec` software.

- [`aztec` genesis file](./genesis.md)
- [HD Wallets for `aztec`](./hd-wallets.md)
- [Ledger Integration for `aztec`](./ledger.md)
- [Service Providers Documentation](./service-providers.md)
- [Reproducible Builds](./reproducible-builds.md)

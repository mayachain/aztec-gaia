package types

import (
	"gitlab.com/mayachain/aztec/x/validatororacle/types/util"
)

const (
	BlocksPerMinute = util.BlocksPerMinute
	BlocksPerHour   = util.BlocksPerHour
	BlocksPerDay    = util.BlocksPerDay
	BlocksPerWeek   = util.BlocksPerWeek
	BlocksPerMonth  = util.BlocksPerMonth
	BlocksPerYear   = util.BlocksPerYear
)

var (
	IsPeriodLastBlock = util.IsPeriodLastBlock
	AddressVerifier   = util.AddressVerifier
)

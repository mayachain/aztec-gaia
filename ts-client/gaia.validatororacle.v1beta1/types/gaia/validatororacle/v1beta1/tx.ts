/* eslint-disable */
import _m0 from "protobufjs/minimal";

export const protobufPackage = "aztec.validatororacle.v1beta1";

/**
 * MsgAggregateValidatorPrevote represents a message to submit
 * aggregate exchange rate prevote.
 */
export interface MsgAggregateValidatorPrevote {
  hash: string;
  feeder: string;
  validator: string;
}

/** MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type. */
export interface MsgAggregateValidatorPrevoteResponse {
}

/**
 * MsgAggregateValidatorVote represents a message to submit
 * aggregate exchange rate vote.
 */
export interface MsgAggregateValidatorVote {
  salt: string;
  validatorsAddress: string;
  feeder: string;
  validator: string;
}

/** MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type. */
export interface MsgAggregateValidatorVoteResponse {
}

/**
 * MsgDelegateFeedConsent represents a message to
 * delegate oracle voting rights to another address.
 */
export interface MsgDelegateFeedConsent {
  operator: string;
  delegate: string;
}

/** MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type. */
export interface MsgDelegateFeedConsentResponse {
}

function createBaseMsgAggregateValidatorPrevote(): MsgAggregateValidatorPrevote {
  return { hash: "", feeder: "", validator: "" };
}

export const MsgAggregateValidatorPrevote = {
  encode(message: MsgAggregateValidatorPrevote, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.hash !== "") {
      writer.uint32(10).string(message.hash);
    }
    if (message.feeder !== "") {
      writer.uint32(18).string(message.feeder);
    }
    if (message.validator !== "") {
      writer.uint32(26).string(message.validator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAggregateValidatorPrevote {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAggregateValidatorPrevote();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.hash = reader.string();
          break;
        case 2:
          message.feeder = reader.string();
          break;
        case 3:
          message.validator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAggregateValidatorPrevote {
    return {
      hash: isSet(object.hash) ? String(object.hash) : "",
      feeder: isSet(object.feeder) ? String(object.feeder) : "",
      validator: isSet(object.validator) ? String(object.validator) : "",
    };
  },

  toJSON(message: MsgAggregateValidatorPrevote): unknown {
    const obj: any = {};
    message.hash !== undefined && (obj.hash = message.hash);
    message.feeder !== undefined && (obj.feeder = message.feeder);
    message.validator !== undefined && (obj.validator = message.validator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAggregateValidatorPrevote>, I>>(object: I): MsgAggregateValidatorPrevote {
    const message = createBaseMsgAggregateValidatorPrevote();
    message.hash = object.hash ?? "";
    message.feeder = object.feeder ?? "";
    message.validator = object.validator ?? "";
    return message;
  },
};

function createBaseMsgAggregateValidatorPrevoteResponse(): MsgAggregateValidatorPrevoteResponse {
  return {};
}

export const MsgAggregateValidatorPrevoteResponse = {
  encode(_: MsgAggregateValidatorPrevoteResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAggregateValidatorPrevoteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAggregateValidatorPrevoteResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgAggregateValidatorPrevoteResponse {
    return {};
  },

  toJSON(_: MsgAggregateValidatorPrevoteResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAggregateValidatorPrevoteResponse>, I>>(
    _: I,
  ): MsgAggregateValidatorPrevoteResponse {
    const message = createBaseMsgAggregateValidatorPrevoteResponse();
    return message;
  },
};

function createBaseMsgAggregateValidatorVote(): MsgAggregateValidatorVote {
  return { salt: "", validatorsAddress: "", feeder: "", validator: "" };
}

export const MsgAggregateValidatorVote = {
  encode(message: MsgAggregateValidatorVote, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.salt !== "") {
      writer.uint32(10).string(message.salt);
    }
    if (message.validatorsAddress !== "") {
      writer.uint32(18).string(message.validatorsAddress);
    }
    if (message.feeder !== "") {
      writer.uint32(26).string(message.feeder);
    }
    if (message.validator !== "") {
      writer.uint32(34).string(message.validator);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAggregateValidatorVote {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAggregateValidatorVote();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.salt = reader.string();
          break;
        case 2:
          message.validatorsAddress = reader.string();
          break;
        case 3:
          message.feeder = reader.string();
          break;
        case 4:
          message.validator = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgAggregateValidatorVote {
    return {
      salt: isSet(object.salt) ? String(object.salt) : "",
      validatorsAddress: isSet(object.validatorsAddress) ? String(object.validatorsAddress) : "",
      feeder: isSet(object.feeder) ? String(object.feeder) : "",
      validator: isSet(object.validator) ? String(object.validator) : "",
    };
  },

  toJSON(message: MsgAggregateValidatorVote): unknown {
    const obj: any = {};
    message.salt !== undefined && (obj.salt = message.salt);
    message.validatorsAddress !== undefined && (obj.validatorsAddress = message.validatorsAddress);
    message.feeder !== undefined && (obj.feeder = message.feeder);
    message.validator !== undefined && (obj.validator = message.validator);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAggregateValidatorVote>, I>>(object: I): MsgAggregateValidatorVote {
    const message = createBaseMsgAggregateValidatorVote();
    message.salt = object.salt ?? "";
    message.validatorsAddress = object.validatorsAddress ?? "";
    message.feeder = object.feeder ?? "";
    message.validator = object.validator ?? "";
    return message;
  },
};

function createBaseMsgAggregateValidatorVoteResponse(): MsgAggregateValidatorVoteResponse {
  return {};
}

export const MsgAggregateValidatorVoteResponse = {
  encode(_: MsgAggregateValidatorVoteResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgAggregateValidatorVoteResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgAggregateValidatorVoteResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgAggregateValidatorVoteResponse {
    return {};
  },

  toJSON(_: MsgAggregateValidatorVoteResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgAggregateValidatorVoteResponse>, I>>(
    _: I,
  ): MsgAggregateValidatorVoteResponse {
    const message = createBaseMsgAggregateValidatorVoteResponse();
    return message;
  },
};

function createBaseMsgDelegateFeedConsent(): MsgDelegateFeedConsent {
  return { operator: "", delegate: "" };
}

export const MsgDelegateFeedConsent = {
  encode(message: MsgDelegateFeedConsent, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.operator !== "") {
      writer.uint32(10).string(message.operator);
    }
    if (message.delegate !== "") {
      writer.uint32(18).string(message.delegate);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDelegateFeedConsent {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDelegateFeedConsent();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.operator = reader.string();
          break;
        case 2:
          message.delegate = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): MsgDelegateFeedConsent {
    return {
      operator: isSet(object.operator) ? String(object.operator) : "",
      delegate: isSet(object.delegate) ? String(object.delegate) : "",
    };
  },

  toJSON(message: MsgDelegateFeedConsent): unknown {
    const obj: any = {};
    message.operator !== undefined && (obj.operator = message.operator);
    message.delegate !== undefined && (obj.delegate = message.delegate);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDelegateFeedConsent>, I>>(object: I): MsgDelegateFeedConsent {
    const message = createBaseMsgDelegateFeedConsent();
    message.operator = object.operator ?? "";
    message.delegate = object.delegate ?? "";
    return message;
  },
};

function createBaseMsgDelegateFeedConsentResponse(): MsgDelegateFeedConsentResponse {
  return {};
}

export const MsgDelegateFeedConsentResponse = {
  encode(_: MsgDelegateFeedConsentResponse, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): MsgDelegateFeedConsentResponse {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseMsgDelegateFeedConsentResponse();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): MsgDelegateFeedConsentResponse {
    return {};
  },

  toJSON(_: MsgDelegateFeedConsentResponse): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<MsgDelegateFeedConsentResponse>, I>>(_: I): MsgDelegateFeedConsentResponse {
    const message = createBaseMsgDelegateFeedConsentResponse();
    return message;
  },
};

/** Msg defines the Msg service. */
export interface Msg {
  /**
   * this line is used by starport scaffolding # proto/tx/rpc
   * AggregateValidatorPrevote defines a method for submitting
   * aggregate validator prevote
   */
  AggregateValidatorPrevote(request: MsgAggregateValidatorPrevote): Promise<MsgAggregateValidatorPrevoteResponse>;
  /**
   * AggregateValidatorVote defines a method for submitting
   * aggregate exchange rate vote
   */
  AggregateValidatorVote(request: MsgAggregateValidatorVote): Promise<MsgAggregateValidatorVoteResponse>;
  /** DelegateFeedConsent defines a method for setting the feeder delegation */
  DelegateFeedConsent(request: MsgDelegateFeedConsent): Promise<MsgDelegateFeedConsentResponse>;
}

export class MsgClientImpl implements Msg {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.AggregateValidatorPrevote = this.AggregateValidatorPrevote.bind(this);
    this.AggregateValidatorVote = this.AggregateValidatorVote.bind(this);
    this.DelegateFeedConsent = this.DelegateFeedConsent.bind(this);
  }
  AggregateValidatorPrevote(request: MsgAggregateValidatorPrevote): Promise<MsgAggregateValidatorPrevoteResponse> {
    const data = MsgAggregateValidatorPrevote.encode(request).finish();
    const promise = this.rpc.request("aztec.validatororacle.v1beta1.Msg", "AggregateValidatorPrevote", data);
    return promise.then((data) => MsgAggregateValidatorPrevoteResponse.decode(new _m0.Reader(data)));
  }

  AggregateValidatorVote(request: MsgAggregateValidatorVote): Promise<MsgAggregateValidatorVoteResponse> {
    const data = MsgAggregateValidatorVote.encode(request).finish();
    const promise = this.rpc.request("aztec.validatororacle.v1beta1.Msg", "AggregateValidatorVote", data);
    return promise.then((data) => MsgAggregateValidatorVoteResponse.decode(new _m0.Reader(data)));
  }

  DelegateFeedConsent(request: MsgDelegateFeedConsent): Promise<MsgDelegateFeedConsentResponse> {
    const data = MsgDelegateFeedConsent.encode(request).finish();
    const promise = this.rpc.request("aztec.validatororacle.v1beta1.Msg", "DelegateFeedConsent", data);
    return promise.then((data) => MsgDelegateFeedConsentResponse.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

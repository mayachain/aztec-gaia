package validatororacle

func Tally(activealidators int, countVotes map[string]int) []string {

	result := []string{}
	threshold := activealidators * 2 / 3

	for address, count := range countVotes {
		if count >= threshold {
			result = append(result, address)
		}
	}

	return result
}

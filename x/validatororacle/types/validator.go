package types

import (
	"strings"

	"gopkg.in/yaml.v2"
)

// String implements fmt.Stringer interface
func (v Validator) String() string {
	out, _ := yaml.Marshal(v)
	return string(out)
}

// Equal implements equal interface
func (v Validator) Equal(v1 *Validator) bool {
	return v.Address == v1.Address
}

// DenomList is array of Denom
type ValidatorList []Validator

// String implements fmt.Stringer interface
func (vl ValidatorList) String() (out string) {
	for _, d := range vl {
		out += d.String() + "\n"
	}
	return strings.TrimSpace(out)
}

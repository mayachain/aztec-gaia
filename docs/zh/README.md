<!-- markdown-link-check-disable -->
# aztec文档

欢迎阅读`aztec`文档。 `aztec`是Cosmos Hub目前的应用程序名。

## 加入Cosmos Hub主网

- [安装`aztec`程序](./aztec-tutorials/installation.md)
- [启动一个全节点并加入主网](./aztec-tutorials/join-mainnet.md)
- [升级成一个验证人节点](./validators/validator-setup.md)

## 加入Cosmos Hub公共测试网

- [加入测试网](./aztec-tutorials/join-testnet.md)

## 部署你自己的`aztec`测试网络

- [部署你自己的`aztec`测试网](./aztec-tutorials/deploy-testnet.md)

## 额外资源

- [验证人介绍](./validators/overview.md)
- [验证人问答](./validators/validator-faq.md)
- [验证人安全性考量](./validators/security.md)

<!-- markdown-link-check-enable -->
/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface ProtobufAny {
  "@type"?: string;
}

export interface RpcStatus {
  /** @format int32 */
  code?: number;
  message?: string;
  details?: ProtobufAny[];
}

export interface V1Beta1AggregateValidatorPrevote {
  hash?: string;
  voter?: string;

  /** @format uint64 */
  submit_block?: string;
}

export interface V1Beta1AggregateValidatorVote {
  validators?: V1Beta1Validator[];
  voter?: string;
}

/**
 * MsgAggregateValidatorPrevoteResponse defines the Msg/AggregateValidatorPrevote response type.
 */
export type V1Beta1MsgAggregateValidatorPrevoteResponse = object;

/**
 * MsgAggregateValidatorVoteResponse defines the Msg/AggregateExchangeRateVote response type.
 */
export type V1Beta1MsgAggregateValidatorVoteResponse = object;

/**
 * MsgDelegateFeedConsentResponse defines the Msg/DelegateFeedConsent response type.
 */
export type V1Beta1MsgDelegateFeedConsentResponse = object;

/**
 * Params defines the parameters for the module.
 */
export interface V1Beta1Params {
  /** @format uint64 */
  vote_period?: string;
  vote_threshold?: string;
  whitelist?: V1Beta1Validator[];
  slash_fraction?: string;

  /** @format uint64 */
  slash_window?: string;
  min_valid_per_window?: string;
}

/**
* QueryActivesResponse is response type for the
Query/Actives RPC method.
*/
export interface V1Beta1QueryActivesResponse {
  /** actives defines a list of the validators which oracle aggreed to whitelist. */
  actives?: string[];
}

/**
* QueryAggregatePrevoteResponse is response type for the
Query/AggregatePrevote RPC method.
*/
export interface V1Beta1QueryAggregatePrevoteResponse {
  aggregate_prevote?: V1Beta1AggregateValidatorPrevote;
}

/**
* QueryAggregatePrevotesResponse is response type for the
Query/AggregatePrevotes RPC method.
*/
export interface V1Beta1QueryAggregatePrevotesResponse {
  /** aggregate_prevotes defines all oracle aggregate prevotes submitted in the current vote period */
  aggregate_prevotes?: V1Beta1AggregateValidatorPrevote[];
}

/**
* QueryAggregateVoteResponse is response type for the
Query/AggregateVote RPC method.
*/
export interface V1Beta1QueryAggregateVoteResponse {
  aggregate_vote?: V1Beta1AggregateValidatorVote;
}

/**
* QueryAggregateVotesResponse is response type for the
Query/AggregateVotes RPC method.
*/
export interface V1Beta1QueryAggregateVotesResponse {
  /** aggregate_votes defines all oracle aggregate votes submitted in the current vote period */
  aggregate_votes?: V1Beta1AggregateValidatorVote[];
}

/**
* QueryFeederDelegationResponse is response type for the
Query/FeederDelegation RPC method.
*/
export interface V1Beta1QueryFeederDelegationResponse {
  /** feeder_addr defines the feeder delegation of a validator */
  feeder_addr?: string;
}

/**
 * QueryParamsResponse is response type for the Query/Params RPC method.
 */
export interface V1Beta1QueryParamsResponse {
  /** Params defines the parameters for the module. */
  params?: V1Beta1Params;
}

export interface V1Beta1Validator {
  address?: string;
}

import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, ResponseType } from "axios";

export type QueryParamsType = Record<string | number, any>;

export interface FullRequestParams extends Omit<AxiosRequestConfig, "data" | "params" | "url" | "responseType"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseType;
  /** request body */
  body?: unknown;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> extends Omit<AxiosRequestConfig, "data" | "cancelToken"> {
  securityWorker?: (
    securityData: SecurityDataType | null,
  ) => Promise<AxiosRequestConfig | void> | AxiosRequestConfig | void;
  secure?: boolean;
  format?: ResponseType;
}

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
}

export class HttpClient<SecurityDataType = unknown> {
  public instance: AxiosInstance;
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private secure?: boolean;
  private format?: ResponseType;

  constructor({ securityWorker, secure, format, ...axiosConfig }: ApiConfig<SecurityDataType> = {}) {
    this.instance = axios.create({ ...axiosConfig, baseURL: axiosConfig.baseURL || "" });
    this.secure = secure;
    this.format = format;
    this.securityWorker = securityWorker;
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  private mergeRequestParams(params1: AxiosRequestConfig, params2?: AxiosRequestConfig): AxiosRequestConfig {
    return {
      ...this.instance.defaults,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.instance.defaults.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  private createFormData(input: Record<string, unknown>): FormData {
    return Object.keys(input || {}).reduce((formData, key) => {
      const property = input[key];
      formData.append(
        key,
        property instanceof Blob
          ? property
          : typeof property === "object" && property !== null
          ? JSON.stringify(property)
          : `${property}`,
      );
      return formData;
    }, new FormData());
  }

  public request = async <T = any, _E = any>({
    secure,
    path,
    type,
    query,
    format,
    body,
    ...params
  }: FullRequestParams): Promise<AxiosResponse<T>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const responseFormat = (format && this.format) || void 0;

    if (type === ContentType.FormData && body && body !== null && typeof body === "object") {
      requestParams.headers.common = { Accept: "*/*" };
      requestParams.headers.post = {};
      requestParams.headers.put = {};

      body = this.createFormData(body as Record<string, unknown>);
    }

    return this.instance.request({
      ...requestParams,
      headers: {
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
        ...(requestParams.headers || {}),
      },
      params: query,
      responseType: responseFormat,
      data: body,
      url: path,
    });
  };
}

/**
 * @title aztec/validatororacle/v1beta1/genesis.proto
 * @version version not set
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregateVote
   * @summary AggregateVote returns an aggregate vote of a validator
   * @request GET:/aztec/validatororacle/valdiators/{validator_addr}/aggregate_vote
   */
  queryAggregateVote = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<V1Beta1QueryAggregateVoteResponse, RpcStatus>({
      path: `/aztec/validatororacle/valdiators/${validatorAddr}/aggregate_vote`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryParams
   * @summary Parameters queries the parameters of the module.
   * @request GET:/aztec/validatororacle/validatororacle/params
   */
  queryParams = (params: RequestParams = {}) =>
    this.request<V1Beta1QueryParamsResponse, RpcStatus>({
      path: `/aztec/validatororacle/validatororacle/params`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryActives
   * @summary Actives returns all whitelisted validators
   * @request GET:/aztec/validatororacle/validatororacle/validators
   */
  queryActives = (params: RequestParams = {}) =>
    this.request<V1Beta1QueryActivesResponse, RpcStatus>({
      path: `/aztec/validatororacle/validatororacle/validators`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryFeederDelegation
   * @summary FeederDelegation returns feeder delegation of a validator
   * @request GET:/aztec/validatororacle/validatororacle/validators/{validator_addr}/feeder
   */
  queryFeederDelegation = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<V1Beta1QueryFeederDelegationResponse, RpcStatus>({
      path: `/aztec/validatororacle/validatororacle/validators/${validatorAddr}/feeder`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregatePrevotes
   * @summary AggregatePrevotes returns aggregate prevotes of all validators
   * @request GET:/aztec/validatororacle/validators/aggregate_prevotes
   */
  queryAggregatePrevotes = (params: RequestParams = {}) =>
    this.request<V1Beta1QueryAggregatePrevotesResponse, RpcStatus>({
      path: `/aztec/validatororacle/validators/aggregate_prevotes`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregateVotes
   * @summary AggregateVotes returns aggregate votes of all validators
   * @request GET:/aztec/validatororacle/validators/aggregate_votes
   */
  queryAggregateVotes = (params: RequestParams = {}) =>
    this.request<V1Beta1QueryAggregateVotesResponse, RpcStatus>({
      path: `/aztec/validatororacle/validators/aggregate_votes`,
      method: "GET",
      format: "json",
      ...params,
    });

  /**
   * No description
   *
   * @tags Query
   * @name QueryAggregatePrevote
   * @summary AggregatePrevote returns an aggregate prevote of a validator
   * @request GET:/aztec/validatororacle/validators/{validator_addr}/aggregate_prevote
   */
  queryAggregatePrevote = (validatorAddr: string, params: RequestParams = {}) =>
    this.request<V1Beta1QueryAggregatePrevoteResponse, RpcStatus>({
      path: `/aztec/validatororacle/validators/${validatorAddr}/aggregate_prevote`,
      method: "GET",
      format: "json",
      ...params,
    });
}

<!--
parent:
  order: false
layout: home
-->

# Cosmos Hub Documentation 

Welcome to the documentation of the **Cosmos Hub application: `aztec`**.

## What is aztec?

- [Intro to the `aztec` software](./getting-started/what-is-aztec.md)
- [Interacting with the `aztecd` binary](./hub-tutorials/aztecd.md)

## Join the Cosmos Hub Mainnet

- [Install the `aztec` application](./getting-started/installation.md)
- [Set up a full node and join the mainnet](./hub-tutorials/join-mainnet.md)
- [Upgrade to a validator node](./validators/validator-setup.md)

## Join the Cosmos Hub Public Testnet

- [Join the testnet](./hub-tutorials/join-testnet.md)

## Setup Your Own `aztec` Testnet

- [Setup your own `aztec` testnet](https://github.com/cosmos/testnets/tree/master/local/previous-local-testnets/theta)

## Additional Resources

- [Validator Resources](./validators/README.md): Contains documentation for `aztec` validators.
- [Delegator Resources](./delegators/README.md): Contains documentation for delegators.
- [Other Resources](./resources/README.md): Contains documentation on `aztecd`, genesis file, service providers, ledger wallets, ...
- [Cosmos Hub Archives](./resources/archives.md): State archives of past iteration of the Cosmos Hub.

# Contribute

See [this file](./DOCS_README.md) for details of the build process and
considerations when making changes.

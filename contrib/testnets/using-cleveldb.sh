#!/bin/bash

make install aztec_BUILD_OPTIONS="cleveldb"

aztecd init "t6" --home ./t6 --chain-id t6

aztecd unsafe-reset-all --home ./t6

mkdir -p ./t6/data/snapshots/metadata.db

aztecd keys add validator --keyring-backend test --home ./t6

aztecd add-genesis-account $(aztecd keys show validator -a --keyring-backend test --home ./t6) 100000000stake --keyring-backend test --home ./t6

aztecd gentx validator 100000000stake --keyring-backend test --home ./t6 --chain-id t6

aztecd collect-gentxs --home ./t6

aztecd start --db_backend cleveldb --home ./t6

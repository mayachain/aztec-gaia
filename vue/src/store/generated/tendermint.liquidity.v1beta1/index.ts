import { Client, registry, MissingWalletError } from 'aztec-v8-client-ts'

import { PoolRecord } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { PoolType } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { Params } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { Pool } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { PoolMetadata } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { PoolBatch } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { DepositMsgState } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { WithdrawMsgState } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"
import { SwapMsgState } from "aztec-v8-client-ts/tendermint.liquidity.v1beta1/types"


export { PoolRecord, PoolType, Params, Pool, PoolMetadata, PoolBatch, DepositMsgState, WithdrawMsgState, SwapMsgState };

function initClient(vuexGetters) {
	return new Client(vuexGetters['common/env/getEnv'], vuexGetters['common/wallet/signer'])
}

function mergeResults(value, next_values) {
	for (let prop of Object.keys(next_values)) {
		if (Array.isArray(next_values[prop])) {
			value[prop]=[...value[prop], ...next_values[prop]]
		}else{
			value[prop]=next_values[prop]
		}
	}
	return value
}

type Field = {
	name: string;
	type: unknown;
}
function getStructure(template) {
	let structure: {fields: Field[]} = { fields: [] }
	for (const [key, value] of Object.entries(template)) {
		let field = { name: key, type: typeof value }
		structure.fields.push(field)
	}
	return structure
}
const getDefaultState = () => {
	return {
				LiquidityPools: {},
				LiquidityPool: {},
				LiquidityPoolByPoolCoinDenom: {},
				LiquidityPoolByReserveAcc: {},
				LiquidityPoolBatch: {},
				PoolBatchSwapMsgs: {},
				PoolBatchSwapMsg: {},
				PoolBatchDepositMsgs: {},
				PoolBatchDepositMsg: {},
				PoolBatchWithdrawMsgs: {},
				PoolBatchWithdrawMsg: {},
				Params: {},
				
				_Structure: {
						PoolRecord: getStructure(PoolRecord.fromPartial({})),
						PoolType: getStructure(PoolType.fromPartial({})),
						Params: getStructure(Params.fromPartial({})),
						Pool: getStructure(Pool.fromPartial({})),
						PoolMetadata: getStructure(PoolMetadata.fromPartial({})),
						PoolBatch: getStructure(PoolBatch.fromPartial({})),
						DepositMsgState: getStructure(DepositMsgState.fromPartial({})),
						WithdrawMsgState: getStructure(WithdrawMsgState.fromPartial({})),
						SwapMsgState: getStructure(SwapMsgState.fromPartial({})),
						
		},
		_Registry: registry,
		_Subscriptions: new Set(),
	}
}

// initial state
const state = getDefaultState()

export default {
	namespaced: true,
	state,
	mutations: {
		RESET_STATE(state) {
			Object.assign(state, getDefaultState())
		},
		QUERY(state, { query, key, value }) {
			state[query][JSON.stringify(key)] = value
		},
		SUBSCRIBE(state, subscription) {
			state._Subscriptions.add(JSON.stringify(subscription))
		},
		UNSUBSCRIBE(state, subscription) {
			state._Subscriptions.delete(JSON.stringify(subscription))
		}
	},
	getters: {
				getLiquidityPools: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.LiquidityPools[JSON.stringify(params)] ?? {}
		},
				getLiquidityPool: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.LiquidityPool[JSON.stringify(params)] ?? {}
		},
				getLiquidityPoolByPoolCoinDenom: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.LiquidityPoolByPoolCoinDenom[JSON.stringify(params)] ?? {}
		},
				getLiquidityPoolByReserveAcc: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.LiquidityPoolByReserveAcc[JSON.stringify(params)] ?? {}
		},
				getLiquidityPoolBatch: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.LiquidityPoolBatch[JSON.stringify(params)] ?? {}
		},
				getPoolBatchSwapMsgs: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchSwapMsgs[JSON.stringify(params)] ?? {}
		},
				getPoolBatchSwapMsg: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchSwapMsg[JSON.stringify(params)] ?? {}
		},
				getPoolBatchDepositMsgs: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchDepositMsgs[JSON.stringify(params)] ?? {}
		},
				getPoolBatchDepositMsg: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchDepositMsg[JSON.stringify(params)] ?? {}
		},
				getPoolBatchWithdrawMsgs: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchWithdrawMsgs[JSON.stringify(params)] ?? {}
		},
				getPoolBatchWithdrawMsg: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.PoolBatchWithdrawMsg[JSON.stringify(params)] ?? {}
		},
				getParams: (state) => (params = { params: {}}) => {
					if (!(<any> params).query) {
						(<any> params).query=null
					}
			return state.Params[JSON.stringify(params)] ?? {}
		},
				
		getTypeStructure: (state) => (type) => {
			return state._Structure[type].fields
		},
		getRegistry: (state) => {
			return state._Registry
		}
	},
	actions: {
		init({ dispatch, rootGetters }) {
			console.log('Vuex module: tendermint.liquidity.v1beta1 initialized!')
			if (rootGetters['common/env/client']) {
				rootGetters['common/env/client'].on('newblock', () => {
					dispatch('StoreUpdate')
				})
			}
		},
		resetState({ commit }) {
			commit('RESET_STATE')
		},
		unsubscribe({ commit }, subscription) {
			commit('UNSUBSCRIBE', subscription)
		},
		async StoreUpdate({ state, dispatch }) {
			state._Subscriptions.forEach(async (subscription) => {
				try {
					const sub=JSON.parse(subscription)
					await dispatch(sub.action, sub.payload)
				}catch(e) {
					throw new Error('Subscriptions: ' + e.message)
				}
			})
		},
		
		
		
		 		
		
		
		async QueryLiquidityPools({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryLiquidityPools(query ?? undefined)).data
				
					
				while (all && (<any> value).pagination && (<any> value).pagination.next_key!=null) {
					let next_values=(await client.TendermintLiquidityV1Beta1.query.queryLiquidityPools({...query ?? {}, 'pagination.key':(<any> value).pagination.next_key} as any)).data
					value = mergeResults(value, next_values);
				}
				commit('QUERY', { query: 'LiquidityPools', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryLiquidityPools', payload: { options: { all }, params: {...key},query }})
				return getters['getLiquidityPools']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryLiquidityPools API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryLiquidityPool({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryLiquidityPool( key.pool_id)).data
				
					
				commit('QUERY', { query: 'LiquidityPool', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryLiquidityPool', payload: { options: { all }, params: {...key},query }})
				return getters['getLiquidityPool']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryLiquidityPool API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryLiquidityPoolByPoolCoinDenom({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryLiquidityPoolByPoolCoinDenom( key.pool_coin_denom)).data
				
					
				commit('QUERY', { query: 'LiquidityPoolByPoolCoinDenom', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryLiquidityPoolByPoolCoinDenom', payload: { options: { all }, params: {...key},query }})
				return getters['getLiquidityPoolByPoolCoinDenom']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryLiquidityPoolByPoolCoinDenom API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryLiquidityPoolByReserveAcc({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryLiquidityPoolByReserveAcc( key.reserve_acc)).data
				
					
				commit('QUERY', { query: 'LiquidityPoolByReserveAcc', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryLiquidityPoolByReserveAcc', payload: { options: { all }, params: {...key},query }})
				return getters['getLiquidityPoolByReserveAcc']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryLiquidityPoolByReserveAcc API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryLiquidityPoolBatch({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryLiquidityPoolBatch( key.pool_id)).data
				
					
				commit('QUERY', { query: 'LiquidityPoolBatch', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryLiquidityPoolBatch', payload: { options: { all }, params: {...key},query }})
				return getters['getLiquidityPoolBatch']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryLiquidityPoolBatch API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchSwapMsgs({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchSwapMsgs( key.pool_id, query ?? undefined)).data
				
					
				while (all && (<any> value).pagination && (<any> value).pagination.next_key!=null) {
					let next_values=(await client.TendermintLiquidityV1Beta1.query.queryPoolBatchSwapMsgs( key.pool_id, {...query ?? {}, 'pagination.key':(<any> value).pagination.next_key} as any)).data
					value = mergeResults(value, next_values);
				}
				commit('QUERY', { query: 'PoolBatchSwapMsgs', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchSwapMsgs', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchSwapMsgs']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchSwapMsgs API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchSwapMsg({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchSwapMsg( key.pool_id,  key.msg_index)).data
				
					
				commit('QUERY', { query: 'PoolBatchSwapMsg', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchSwapMsg', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchSwapMsg']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchSwapMsg API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchDepositMsgs({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchDepositMsgs( key.pool_id, query ?? undefined)).data
				
					
				while (all && (<any> value).pagination && (<any> value).pagination.next_key!=null) {
					let next_values=(await client.TendermintLiquidityV1Beta1.query.queryPoolBatchDepositMsgs( key.pool_id, {...query ?? {}, 'pagination.key':(<any> value).pagination.next_key} as any)).data
					value = mergeResults(value, next_values);
				}
				commit('QUERY', { query: 'PoolBatchDepositMsgs', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchDepositMsgs', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchDepositMsgs']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchDepositMsgs API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchDepositMsg({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchDepositMsg( key.pool_id,  key.msg_index)).data
				
					
				commit('QUERY', { query: 'PoolBatchDepositMsg', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchDepositMsg', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchDepositMsg']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchDepositMsg API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchWithdrawMsgs({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchWithdrawMsgs( key.pool_id, query ?? undefined)).data
				
					
				while (all && (<any> value).pagination && (<any> value).pagination.next_key!=null) {
					let next_values=(await client.TendermintLiquidityV1Beta1.query.queryPoolBatchWithdrawMsgs( key.pool_id, {...query ?? {}, 'pagination.key':(<any> value).pagination.next_key} as any)).data
					value = mergeResults(value, next_values);
				}
				commit('QUERY', { query: 'PoolBatchWithdrawMsgs', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchWithdrawMsgs', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchWithdrawMsgs']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchWithdrawMsgs API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryPoolBatchWithdrawMsg({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryPoolBatchWithdrawMsg( key.pool_id,  key.msg_index)).data
				
					
				commit('QUERY', { query: 'PoolBatchWithdrawMsg', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryPoolBatchWithdrawMsg', payload: { options: { all }, params: {...key},query }})
				return getters['getPoolBatchWithdrawMsg']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryPoolBatchWithdrawMsg API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		
		
		 		
		
		
		async QueryParams({ commit, rootGetters, getters }, { options: { subscribe, all} = { subscribe:false, all:false}, params, query=null }) {
			try {
				const key = params ?? {};
				const client = initClient(rootGetters);
				let value= (await client.TendermintLiquidityV1Beta1.query.queryParams()).data
				
					
				commit('QUERY', { query: 'Params', key: { params: {...key}, query}, value })
				if (subscribe) commit('SUBSCRIBE', { action: 'QueryParams', payload: { options: { all }, params: {...key},query }})
				return getters['getParams']( { params: {...key}, query}) ?? {}
			} catch (e) {
				throw new Error('QueryClient:QueryParams API Node Unavailable. Could not perform query: ' + e.message)
				
			}
		},
		
		
		async sendMsgWithdrawWithinBatch({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.TendermintLiquidityV1Beta1.tx.sendMsgWithdrawWithinBatch({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgWithdrawWithinBatch:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgWithdrawWithinBatch:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgDepositWithinBatch({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.TendermintLiquidityV1Beta1.tx.sendMsgDepositWithinBatch({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDepositWithinBatch:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgDepositWithinBatch:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgCreatePool({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.TendermintLiquidityV1Beta1.tx.sendMsgCreatePool({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreatePool:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgCreatePool:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		async sendMsgSwapWithinBatch({ rootGetters }, { value, fee = [], memo = '' }) {
			try {
				const client=await initClient(rootGetters)
				const result = await client.TendermintLiquidityV1Beta1.tx.sendMsgSwapWithinBatch({ value, fee: {amount: fee, gas: "200000"}, memo })
				return result
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgSwapWithinBatch:Init Could not initialize signing client. Wallet is required.')
				}else{
					throw new Error('TxClient:MsgSwapWithinBatch:Send Could not broadcast Tx: '+ e.message)
				}
			}
		},
		
		async MsgWithdrawWithinBatch({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.TendermintLiquidityV1Beta1.tx.msgWithdrawWithinBatch({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgWithdrawWithinBatch:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgWithdrawWithinBatch:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgDepositWithinBatch({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.TendermintLiquidityV1Beta1.tx.msgDepositWithinBatch({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgDepositWithinBatch:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgDepositWithinBatch:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgCreatePool({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.TendermintLiquidityV1Beta1.tx.msgCreatePool({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgCreatePool:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgCreatePool:Create Could not create message: ' + e.message)
				}
			}
		},
		async MsgSwapWithinBatch({ rootGetters }, { value }) {
			try {
				const client=initClient(rootGetters)
				const msg = await client.TendermintLiquidityV1Beta1.tx.msgSwapWithinBatch({value})
				return msg
			} catch (e) {
				if (e == MissingWalletError) {
					throw new Error('TxClient:MsgSwapWithinBatch:Init Could not initialize signing client. Wallet is required.')
				} else{
					throw new Error('TxClient:MsgSwapWithinBatch:Create Could not create message: ' + e.message)
				}
			}
		},
		
	}
}

package keeper

import (
	_ "fmt"
	_ "strings"

	sdk "github.com/cosmos/cosmos-sdk/types"
	_ "github.com/cosmos/cosmos-sdk/x/bank/types"
	"gitlab.com/mayachain/aztec/x/validatororacle/types"
)

// OrganizeBallotByDenom collects all oracle votes for the period, categorized by the votes' denom parameter
func (k Keeper) OrganizeBallotByAddress(ctx sdk.Context, validatorClaimMap map[string]types.Claim) (votes map[string][]string, countVotes map[string]int) {
	votes = map[string][]string{}
	countVotes = map[string]int{}

	for key := range validatorClaimMap {
		votes[key] = []string{}
	}

	// Organize aggregate votes
	aggregateHandler := func(voterAddr sdk.ValAddress, vote types.AggregateValidatorVote) (stop bool) {
		// organize ballot only for the active validators
		_, ok := validatorClaimMap[vote.Voter]

		if ok {
			//power := claim.Power
			for _, validator := range vote.Validators {

				votes[voterAddr.String()] = append(votes[voterAddr.String()], validator.Address)
				countVotes[validator.Address] += 1
			}

		}

		return false
	}

	k.IterateAggregateValidatorVotes(ctx, aggregateHandler)

	// sort created ballot
	//countVotes := map[string]int{}
	// for address, ballot := range votes {
	// 	votes[address] = ballot

	// }

	return
}

// ClearBallots clears all tallied prevotes and votes from the store
func (k Keeper) ClearBallots(ctx sdk.Context, votePeriod uint64) {
	// Clear all aggregate prevotes
	k.IterateAggregateValidatorPrevotes(ctx, func(voterAddr sdk.ValAddress, aggregatePrevote types.AggregateValidatorPrevote) (stop bool) {
		if ctx.BlockHeight() > int64(aggregatePrevote.SubmitBlock+votePeriod) {
			k.DeleteAggregateValidatorPrevote(ctx, voterAddr)
		}

		return false
	})

	// Clear all aggregate votes
	k.IterateAggregateValidatorVotes(ctx, func(voterAddr sdk.ValAddress, aggregateVote types.AggregateValidatorVote) (stop bool) {
		k.DeleteAggregateValidatorVote(ctx, voterAddr)
		return false
	})
}

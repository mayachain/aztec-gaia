#!/bin/bash

# initialize config
if [[ ! -f "/root/.aztec/config/app.toml" ]]; then
	cp /etc/aztec/app.toml /root/.aztec/config/app.toml
fi

exec aztecd start --log_format json --rpc.laddr tcp://0.0.0.0:26657 --x-crisis-skip-assert-invariants "$@"

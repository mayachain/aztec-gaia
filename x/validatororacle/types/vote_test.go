package types

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParseValidators(t *testing.T) {
	valid := "cosmosvaloper1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mces8lvs2k"
	_, err := ParseValidators(valid)
	require.NoError(t, err)

	duplicatedAddress := "cosmosvalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly,terravalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly"
	_, err = ParseValidators(duplicatedAddress)
	require.Error(t, err)

	invalidAddress := "cosmosvalconspub1zcjduepqs5s0vddx5m65h5ntjzwd0x8g3245rgrytpds4ds7vdtlwx06mcesmnkzly"
	_, err = ParseValidators(invalidAddress)
	require.Error(t, err)

	emptyList := ""
	_, err = ParseValidators(emptyList)
	require.NoError(t, err)

}

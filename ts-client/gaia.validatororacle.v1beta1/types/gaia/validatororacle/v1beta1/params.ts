/* eslint-disable */
import Long from "long";
import _m0 from "protobufjs/minimal";

export const protobufPackage = "aztec.validatororacle.v1beta1";

/** Params defines the parameters for the module. */
export interface Params {
  votePeriod: number;
  voteThreshold: string;
  whitelist: Validator[];
  slashFraction: string;
  slashWindow: number;
  minValidPerWindow: string;
}

/** Validator define the parameters for each validator */
export interface Validator {
  address: string;
}

/**
 * struct for aggregate prevoting on the ValidatorVote.
 * The purpose of aggregate prevote is to hide vote validators whitelist with hash
 * which is formatted as hex string in SHA256("{salt}:{validator-addresss},...,{validator-addresss}:{voter}")
 */
export interface AggregateValidatorPrevote {
  hash: string;
  voter: string;
  submitBlock: number;
}

/** MsgAggregateValidatorVote - struct for voting on */
export interface AggregateValidatorVote {
  validators: Validator[];
  voter: string;
}

function createBaseParams(): Params {
  return { votePeriod: 0, voteThreshold: "", whitelist: [], slashFraction: "", slashWindow: 0, minValidPerWindow: "" };
}

export const Params = {
  encode(message: Params, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.votePeriod !== 0) {
      writer.uint32(8).uint64(message.votePeriod);
    }
    if (message.voteThreshold !== "") {
      writer.uint32(18).string(message.voteThreshold);
    }
    for (const v of message.whitelist) {
      Validator.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    if (message.slashFraction !== "") {
      writer.uint32(34).string(message.slashFraction);
    }
    if (message.slashWindow !== 0) {
      writer.uint32(40).uint64(message.slashWindow);
    }
    if (message.minValidPerWindow !== "") {
      writer.uint32(50).string(message.minValidPerWindow);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Params {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseParams();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.votePeriod = longToNumber(reader.uint64() as Long);
          break;
        case 2:
          message.voteThreshold = reader.string();
          break;
        case 3:
          message.whitelist.push(Validator.decode(reader, reader.uint32()));
          break;
        case 4:
          message.slashFraction = reader.string();
          break;
        case 5:
          message.slashWindow = longToNumber(reader.uint64() as Long);
          break;
        case 6:
          message.minValidPerWindow = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Params {
    return {
      votePeriod: isSet(object.votePeriod) ? Number(object.votePeriod) : 0,
      voteThreshold: isSet(object.voteThreshold) ? String(object.voteThreshold) : "",
      whitelist: Array.isArray(object?.whitelist) ? object.whitelist.map((e: any) => Validator.fromJSON(e)) : [],
      slashFraction: isSet(object.slashFraction) ? String(object.slashFraction) : "",
      slashWindow: isSet(object.slashWindow) ? Number(object.slashWindow) : 0,
      minValidPerWindow: isSet(object.minValidPerWindow) ? String(object.minValidPerWindow) : "",
    };
  },

  toJSON(message: Params): unknown {
    const obj: any = {};
    message.votePeriod !== undefined && (obj.votePeriod = Math.round(message.votePeriod));
    message.voteThreshold !== undefined && (obj.voteThreshold = message.voteThreshold);
    if (message.whitelist) {
      obj.whitelist = message.whitelist.map((e) => e ? Validator.toJSON(e) : undefined);
    } else {
      obj.whitelist = [];
    }
    message.slashFraction !== undefined && (obj.slashFraction = message.slashFraction);
    message.slashWindow !== undefined && (obj.slashWindow = Math.round(message.slashWindow));
    message.minValidPerWindow !== undefined && (obj.minValidPerWindow = message.minValidPerWindow);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Params>, I>>(object: I): Params {
    const message = createBaseParams();
    message.votePeriod = object.votePeriod ?? 0;
    message.voteThreshold = object.voteThreshold ?? "";
    message.whitelist = object.whitelist?.map((e) => Validator.fromPartial(e)) || [];
    message.slashFraction = object.slashFraction ?? "";
    message.slashWindow = object.slashWindow ?? 0;
    message.minValidPerWindow = object.minValidPerWindow ?? "";
    return message;
  },
};

function createBaseValidator(): Validator {
  return { address: "" };
}

export const Validator = {
  encode(message: Validator, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.address !== "") {
      writer.uint32(10).string(message.address);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): Validator {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseValidator();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.address = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): Validator {
    return { address: isSet(object.address) ? String(object.address) : "" };
  },

  toJSON(message: Validator): unknown {
    const obj: any = {};
    message.address !== undefined && (obj.address = message.address);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<Validator>, I>>(object: I): Validator {
    const message = createBaseValidator();
    message.address = object.address ?? "";
    return message;
  },
};

function createBaseAggregateValidatorPrevote(): AggregateValidatorPrevote {
  return { hash: "", voter: "", submitBlock: 0 };
}

export const AggregateValidatorPrevote = {
  encode(message: AggregateValidatorPrevote, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.hash !== "") {
      writer.uint32(10).string(message.hash);
    }
    if (message.voter !== "") {
      writer.uint32(18).string(message.voter);
    }
    if (message.submitBlock !== 0) {
      writer.uint32(24).uint64(message.submitBlock);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AggregateValidatorPrevote {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAggregateValidatorPrevote();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.hash = reader.string();
          break;
        case 2:
          message.voter = reader.string();
          break;
        case 3:
          message.submitBlock = longToNumber(reader.uint64() as Long);
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): AggregateValidatorPrevote {
    return {
      hash: isSet(object.hash) ? String(object.hash) : "",
      voter: isSet(object.voter) ? String(object.voter) : "",
      submitBlock: isSet(object.submitBlock) ? Number(object.submitBlock) : 0,
    };
  },

  toJSON(message: AggregateValidatorPrevote): unknown {
    const obj: any = {};
    message.hash !== undefined && (obj.hash = message.hash);
    message.voter !== undefined && (obj.voter = message.voter);
    message.submitBlock !== undefined && (obj.submitBlock = Math.round(message.submitBlock));
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<AggregateValidatorPrevote>, I>>(object: I): AggregateValidatorPrevote {
    const message = createBaseAggregateValidatorPrevote();
    message.hash = object.hash ?? "";
    message.voter = object.voter ?? "";
    message.submitBlock = object.submitBlock ?? 0;
    return message;
  },
};

function createBaseAggregateValidatorVote(): AggregateValidatorVote {
  return { validators: [], voter: "" };
}

export const AggregateValidatorVote = {
  encode(message: AggregateValidatorVote, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    for (const v of message.validators) {
      Validator.encode(v!, writer.uint32(10).fork()).ldelim();
    }
    if (message.voter !== "") {
      writer.uint32(18).string(message.voter);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): AggregateValidatorVote {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseAggregateValidatorVote();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.validators.push(Validator.decode(reader, reader.uint32()));
          break;
        case 2:
          message.voter = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): AggregateValidatorVote {
    return {
      validators: Array.isArray(object?.validators) ? object.validators.map((e: any) => Validator.fromJSON(e)) : [],
      voter: isSet(object.voter) ? String(object.voter) : "",
    };
  },

  toJSON(message: AggregateValidatorVote): unknown {
    const obj: any = {};
    if (message.validators) {
      obj.validators = message.validators.map((e) => e ? Validator.toJSON(e) : undefined);
    } else {
      obj.validators = [];
    }
    message.voter !== undefined && (obj.voter = message.voter);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<AggregateValidatorVote>, I>>(object: I): AggregateValidatorVote {
    const message = createBaseAggregateValidatorVote();
    message.validators = object.validators?.map((e) => Validator.fromPartial(e)) || [];
    message.voter = object.voter ?? "";
    return message;
  },
};

declare var self: any | undefined;
declare var window: any | undefined;
declare var global: any | undefined;
var globalThis: any = (() => {
  if (typeof globalThis !== "undefined") {
    return globalThis;
  }
  if (typeof self !== "undefined") {
    return self;
  }
  if (typeof window !== "undefined") {
    return window;
  }
  if (typeof global !== "undefined") {
    return global;
  }
  throw "Unable to locate global object";
})();

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Array<infer U> ? Array<DeepPartial<U>> : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function longToNumber(long: Long): number {
  if (long.gt(Number.MAX_SAFE_INTEGER)) {
    throw new globalThis.Error("Value is larger than Number.MAX_SAFE_INTEGER");
  }
  return long.toNumber();
}

if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}

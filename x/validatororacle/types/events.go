package types

// Oracle module event types
const (
	EventTypeValidatorsWhitelistUpdate = "validators_whitelist_update"
	EventTypePrevote                   = "prevote"
	EventTypeVote                      = "vote"
	EventTypeFeedDelegate              = "feed_delegate"
	EventTypeAggregatePrevote          = "aggregate_prevote"
	EventTypeAggregateVote             = "aggregate_vote"

	AttributeKeyAddress    = "address"
	AttributeKeyVoter      = "voter"
	AttributeKeyValidator  = "validator whitelist"
	AttributeKeyValidators = "validators whitelisted"
	AttributeKeyOperator   = "operator"
	AttributeKeyFeeder     = "feeder"

	AttributeValueCategory = ModuleName
)

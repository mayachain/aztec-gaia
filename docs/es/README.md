<!-- markdown-link-check-disable -->
# Documentación para el Hub de Cosmos

Bienvenido a la documentación de la **aplicación para el Hub de Cosmos: `aztec`**

## ¿Qué es aztec?

- [Introducción al software `aztec`](./aztec-tutorials/what-is-aztec.md)

## Únase a la red principal del Hub de Cosmos

- [Instale la aplicación de `aztec`](./aztec-tutorials/installation.md)

- [Prepare un full node y únase a la mainnet](./aztec-tutorials/join-mainnet.md)

- [Actualice para ser un nodo validador](./validators/validator-setup.md)

## Únase a la testnet pública del hub de Cosmos

- [Únase a la testnet](./hub-tutorials/join-testnet.md)

## Prepare su propia Testnet de `aztec`

- [Prepare su propia Testnet de `aztec`](../hub-tutorials/deploy-testnet.md)

## Recursos adicionales

- [Recursos para validadores](./validators/README.md): Contiene documentación de `aztec` para los validadores.

- [Recursos para delegadores](./delegators/README.md): Contiene documentación para los delegadores.

- [Otros recursos](./resources/README.md): Contiene documentación de `aztecd`, archivo para el génesis, proveedores de servicios, wallet ledger ...

- [Artículos sobre el Hub de Cosmos](./resources/archives.md): Archivos históricos de la pasada edición del Hub de Cosmos.

# Contribución

Consulte [este archivo](./DOCS_README.md) para conocer los detalles del proceso de elaboración y las consideraciones a la hora de hacer cambios.

# Versión

Esta documentación ha construida usando el siguiente commit:

<!-- markdown-link-check-enable -->
